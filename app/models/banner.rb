class Banner < ApplicationRecord
  mount_uploader :image, BannerImageUploader

  belongs_to :campaign
  has_many :page_visits, dependent: :destroy

  validates :landing_page, format: { with: URI::regexp(%w(http https)), allow_blank: true }
  validates_presence_of :image

  delegate :landing_page, to: :campaign, allow_nil: true, prefix: true

  def current_landing_page
    self.landing_page.presence || self.campaign_landing_page
  end
end
