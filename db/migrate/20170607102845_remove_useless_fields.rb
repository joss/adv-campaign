class RemoveUselessFields < ActiveRecord::Migration[5.1]
  def change
    remove_column :banners, :views_count, :integer
    remove_column :banners, :clicks_count, :integer

    remove_column :campaigns, :html_code, :text
  end
end
