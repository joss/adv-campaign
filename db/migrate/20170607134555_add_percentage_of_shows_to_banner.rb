class AddPercentageOfShowsToBanner < ActiveRecord::Migration[5.1]
  def change
    add_column :banners, :percentage_of_shows, :integer, null: false, default: 0
  end
end
