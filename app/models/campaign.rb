class Campaign < ApplicationRecord
  has_many :banners, dependent: :destroy
  has_many :page_visits, through: :banners

  validates :title, presence: true, uniqueness: true
  validates :landing_page,
    presence: true,
    format: { with: URI::regexp(%w(http https)), allow_blank: true }

  def self.banner_weights_sum
    Campaign.joins(:banners).group(:id).sum(:percentage_of_shows)
  end

  def weights_sum_correct?
    self.class.banner_weights_sum[self.id] == 100
  end

  def load_banners
    self.banners.all.load
  end

  def campaign_aggregated_data
    unique_users_relation = self.page_visits.group(:banner_id).distinct(:source_ip)
    most_visited_domain_relation = self.page_visits.group(:source_domain)

    {
      unique_users_clicked: unique_users_relation.where('clicks_count > 0').count(:source_ip),
      unique_users_viewed: unique_users_relation.where('views_count > 0').count(:source_ip),
      most_clicked_domain: most_visited_domain_relation.maximum(:clicks_count).max_by{ |_, v| v }.try(:first),
      most_viewed_domain: most_visited_domain_relation.maximum(:views_count).max_by{ |_, v| v }.try(:first)
    }
  end
end
