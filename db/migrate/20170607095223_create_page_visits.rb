class CreatePageVisits < ActiveRecord::Migration[5.1]
  def change
    create_table :page_visits do |t|
      t.belongs_to :banner, foreign_key: true

      t.integer :views_count, null: false, default: 0
      t.integer :clicks_count, null: false, default: 0

      t.string :source_ip
      t.string :source_domain

      t.timestamps
    end
  end
end
