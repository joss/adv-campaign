class CampaignsController < ApplicationController
  before_action :set_campaign, except: [:index, :new, :create]

  def index
    @campaigns = Campaign.order(:title).load
    @weights_sum = Campaign.banner_weights_sum
  end

  def show
    @banners = @campaign.load_banners
    @campaign_aggregated_data = @campaign.campaign_aggregated_data

    @banner = @campaign.banners.new

    flash.now[:error] = "Total weights sum of all banners must equal 100" unless @campaign.weights_sum_correct?
  end

  def new
    @campaign = Campaign.new
  end

  def edit
  end

  def create
    @campaign = Campaign.new(campaign_params)

    if @campaign.save
      redirect_to campaigns_path, notice: 'Campaign was successfully created.'
    else
      render :new
    end
  end

  def update
    if @campaign.update(campaign_params)
      redirect_to campaigns_path, notice: 'Campaign was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @campaign.destroy
    redirect_to campaigns_path, notice: 'Campaign was successfully destroyed.'
  end

  def display_banner
    @banner = FindRandomBanner.new(@campaign).perform
    @page_visit = ViewBanner.new(@banner, request.remote_ip, request.referer).perform

    render layout: false
  end

  def html_code
    render layout: false
  end

  private
    def set_campaign
      @campaign = Campaign.find(params[:id])
    end

    def campaign_params
      params.require(:campaign).permit(:title, :landing_page, :html_code)
    end
end
