class BannersController < ApplicationController
  before_action :set_campaign
  before_action :set_banner, except: [:new, :create]

  def new
    @banner = @campaign.banners.new
  end

  def edit
  end

  def create
    @banner = @campaign.banners.new(banner_params)
    @banners = @campaign.load_banners
    @campaign_aggregated_data = @campaign.campaign_aggregated_data

    if @banner.save
      redirect_to @campaign, notice: 'Banner was successfully created.'
    else
      render 'campaigns/show'
    end
  end

  def update
    if @banner.update(banner_params)
      redirect_to @campaign, notice: 'Banner was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @banner.destroy
    redirect_to @campaign, notice: 'Banner was successfully destroyed.'
  end

  def click
    @page_visit = ClickBanner.new(@banner, request.remote_ip, request.referer).perform

    redirect_to @banner.current_landing_page
  end

  private
    def set_campaign
      @campaign = Campaign.find(params[:campaign_id])
    end

    def set_banner
      @banner = @campaign.banners.find(params[:id])
    end

    def banner_params
      params.require(:banner).permit(:percentage_of_shows, :landing_page, :image)
    end
end
