Rails.application.routes.draw do
  resources :campaigns do
    get :display_banner, on: :member
    get :html_code, on: :member

    resources :banners, except: [:index, :show, :new] do
      get :click, on: :member
    end
  end

  root to: 'campaigns#index'
end
