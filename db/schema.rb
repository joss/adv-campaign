# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170607134555) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "banners", force: :cascade do |t|
    t.bigint "campaign_id"
    t.string "landing_page"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "percentage_of_shows", default: 0, null: false
    t.index ["campaign_id"], name: "index_banners_on_campaign_id"
  end

  create_table "campaigns", force: :cascade do |t|
    t.string "title"
    t.string "landing_page"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "page_visits", force: :cascade do |t|
    t.bigint "banner_id"
    t.integer "views_count", default: 0, null: false
    t.integer "clicks_count", default: 0, null: false
    t.string "source_ip"
    t.string "source_domain"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["banner_id"], name: "index_page_visits_on_banner_id"
  end

  add_foreign_key "banners", "campaigns"
  add_foreign_key "page_visits", "banners"
end
