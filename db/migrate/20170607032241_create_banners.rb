class CreateBanners < ActiveRecord::Migration[5.1]
  def change
    create_table :banners do |t|
      t.belongs_to :campaign, foreign_key: true

      t.string :landing_page
      t.string :image

      t.integer :views_count, null: false, default: 0
      t.integer :clicks_count, null: false, default: 0

      t.timestamps
    end
  end
end
