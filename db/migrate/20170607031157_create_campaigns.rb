class CreateCampaigns < ActiveRecord::Migration[5.1]
  def change
    create_table :campaigns do |t|
      t.string :title
      t.string :landing_page
      t.text :html_code

      t.timestamps
    end
  end
end
