class ViewBanner < Struct.new(:banner, :remote_ip, :referer)
  def perform
    PageVisit.with_advisory_lock(lock_key) do
      @visit = self.banner.page_visits.where(
        source_ip: self.remote_ip,
        source_domain: parse_referer
      ).first_or_initialize

      @visit.views_count += 1
      @visit.save
      @visit
    end
  end

  private
    def parse_referer
      URI.parse(self.referer.to_s).host
    end

    def lock_key
      'update-views-count-#{self.banner.id}-#{self.remote_ip}-#{self.referer}'
    end
end
