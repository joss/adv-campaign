class FindRandomBanner < Struct.new(:campaign)
  def perform
    weights = if self.campaign.weights_sum_correct?
      Hash[self.campaign.banners.pluck('id, percentage_of_shows * 0.01')]
    else
      default_weight = 100 / self.campaign.banners.count
      self.campaign.banners.pluck(:id).each_with_object({}) do |id, memo|
        memo[id] = default_weight * 0.01
      end
    end

    offset = 0.0
    current_random = rand

    ranges = Hash[weights.map{ |id, perc| [offset += perc, id] }]
    current_banner_id = ranges.detect{ |id, _| id > current_random }.try(:last).to_f

    Banner.find(current_banner_id)
  end
end
